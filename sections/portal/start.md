# Portal

## For users

### Registering a new user

#### Open the Portal

* Click on **Register** in the top right corner.

<img src="images/portal/user/01_user_start.png"></img>

#### Fill in the form

* Enter your data.
* Read the privacy policy.
* Consent to the privacy policy.
* Click Register.

<img src="images/portal/user/03_portal_staging_register_form.png"></img>

#### Receive verification email

* You will receive an email with a verification link.
* Click on the verification link.

<img src="images/portal/user/06_portal_staging_verify.png"></img>

#### Complete registration

* You will be forwarded to the start page.
* Click **Login**.

<img src="images/portal/user/01_user_start.png"></img>

### Joining an organization

#### Login with user

* Click **Login** in the top right corner of the Portal.
* Enter your email address and password.
* Click **Login**.

<img src="images/portal/user/06_portal_staging_login.png"></img>

#### Request to join organization

* Click on the **Organizations** tab at the top.
* Choose organization from list.

<img src="images/portal/user/00_1_join_organisation.png"></img>


* Click on the **Join** button of the organization you want to join. Please note, that the **Join** button only appears after login.

<img src="images/portal/user/00_2_join_organisation.png"></img>

* Your organization manager will be notified about your request.

<img src="images/portal/user/00_3_join_organisation.png"></img>

#### Approval of request

* As soon as your organization manager accepts your request, you will be notified about the approval.
* You are now a member of the organization.

<img src="images/portal/user/00_4_join_organisation.png"></img>

* You are also able to see the approval of your membership request in your notifications.

<img src="images/portal/user/00_5_join_organisation.png"></img>

### Requesting a user role

* Click on your name in the portal.
* Click on **Active Organization**.

<img src="images/portal/user/05_1_user_role_request.png"></img>

* Click on **Request role change**.
* Here you can choose between different roles.

<img src="images/portal/user/05_2_user_role_request.png"></img>

User roles:

* **Organization Manager:** Manage members and their roles in the organization. Role is available for all organizations.
* **Pathologist:** Access to organization's Workbench for diagnostic use of AI Applications. Role is only available for organizations in category "Pathology Laboratory".
* **Clinical Data Manager:** Access to organizations Data Manager for uploading and editing of histopathological cases and slide data. Role is only available for organizations in category "Pathology Laboratory".
* **Market Survey Maintainer:** Manage product data of AI Applications for market survey. Role is only available for organizations in category "Market Survey Paticipant". 

### Switching organizations

* If you are a member of multiple organizations, you can switch your **Active Organization** in the Portal by clicking on the name of the currently active organization.
* You will be prompted with a selection screen.
* Organization specific actions performed via the EMPAIA Portal will always be applied in the context of the currently active organization. Please check the organization name next to your profile picture and switch organizations accordingly.

<img src="images/portal/user/00_4_register_organisation.png"></img>

## For organization managers

### Managing users

#### View administration panel

* Click on your name in the top right.
* Click on **Admistration** to see the administration panel.

<img src="images/portal/organization/02_0_my_administration_panel.png"></img>

#### Manage user roles

* Here you can view a list of members of your organization.

<img src="images/portal/organization/02_1_org_admin_panel_manage_users.png"></img>

* Click on the three dots on the right side of a selected member.
* Click **Edit roles** to change member role.

<img src="images/portal/organization/02_2_org_admin_panel_manage_users.png"></img>

* Here you can choose different roles for members.
* Click **Confirm** to save your settings.

<img src="images/portal/organization/02_3_org_admin_panel_manage_users.png"></img>

**Organization Manager:** Manage members and their roles in the organization. Role is available for all organizations.

**Pathologist:** Access to organization's Workbench for diagnostic use of AI Applications. Role is only available for organizations in category "Pathology Laboratory".

**Clinical Data Manager:** Access to organizations Data Manager for uploading and editing of histopathological cases and slide data. Role is only available for organizations in category "Pathology Laboratory".

**Market Survey Maintainer:** Manage product data of AI Applications for market survey. Role is only available for organizations in category "Market Survey Paticipant". 

#### Remove a user

* Navigate to your administration panel.
* Click on the three dots on the right side of a selected member.

<img src="images/portal/organization/02_1_org_admin_panel_manage_users.png"></img>

* Click on **Remove** to remove user from your organization.

<img src="images/portal/organization/02_2_org_admin_panel_manage_users.png"></img>

#### Membership requests

* Review membership requests thoroughly and only grant access for trusted accounts.
* Accounts can be identified by their email address, that has been validated during account creation.
* In order to accept a membership request, click on the button with the **tick**.

<img src="images/portal/organization/03_1_org_admin_panel_membership_request.png"></img>

* Click on **Confirm** to confirm membership request of the user. 

<img src="images/portal/organization/03_2_org_admin_panel_membership_request.png"></img>

### Managing organization

#### Edit the organization

* Click **edit** to change details of your organization.

<img src="images/portal/organization/04_org_admin_panel_edit_organisation.png"></img>

#### Request a category change

* Click **Request organization category change**

<img src="images/portal/organization/05_1_org_admin_panel_request_category_change.png"></img>

* Choose organization categories.
* Organization categories provide access to advanced features and specific categories will only be granted if the organization is allowed to receive these privileges.
* Click on **Confirm** to request an organization category change.

Organization categories:

* **Pathology Laboratory**: Only allowed for pathology laboratories, that have a contractual relationship with EMPAIA to evaluate AI Apps from the EMPAIA Marketplace. Grants access to user roles Pathologist and Data Manager.
* **AI App Vendor**: Only allowed for AI App vendors, that provide EMPAIA compliant AI Apps via the EMPAIA Marketplace. Does not yet grant access to specific user roles.
* **Compute Cluster Operator**: only allowed for official EMPAIA compute cluster operators, that provide access to a shared Job Execution Service to run AI Apps. Please contact the EMPAIA team for details.
* **Market Survey Participant**: only allowed for participants of the EMPAIA Market Survey. Grants access to user role Market Survey Maintainer to edit public information about the organization's products.

<img src="images/portal/organization/05_3_org_admin_panel_request_category_change.png"></img>

* The request will be processed by an EMPAIA Portal Moderator.
* You will receive an email notification as soon as the request has been reviewed.

#### Revoke a category change request

* Click on the three dots of one specific category change request.
* Click on **Revoke request**  to delete organization category request.
* The request will not be reviewed by an EMPAIA Portal Moderator anymore.

<img src="images/portal/organization/05_2_org_admin_panel_request_category_change.png"></img>

## For new organizations

### Create a new organization

* Click on **Create Organization** 

<img src="images/portal/user/00_1_register_organisation.png"></img>

* Enter organization name.

<img src="images/portal/user/00_2_register_organisation.png"></img>

* Enter organization details.
* Choose organization categories.
* Organization categories provide access to advanced features and specific categories will only be granted if the organization is allowed to receive these privileges.

Organization categories:

* **Pathology Laboratory**: Only allowed for pathology laboratories, that have a contractual relationship with EMPAIA to evaluate AI Apps from the EMPAIA Marketplace. Grants access to user roles Pathologist and Data Manager.
* **AI App Vendor**: Only allowed for AI App vendors, that provide EMPAIA compliant AI Apps via the EMPAIA Marketplace. Does not yet grant access to specific user roles.
* **Compute Cluster Operator**: only allowed for official EMPAIA compute cluster operators, that provide access to a shared Job Execution Service to run AI Apps. Please contact the EMPAIA team for details.
* **Market Survey Participant**: only allowed for participants of the EMPAIA Market Survey. Grants access to user role Market Survey Maintainer to edit public information about the organization's products.

<img src="images/portal/user/00_3_register_organisation.png"></img>

* Click on **Send** to request the creation of a new organization.
* This request will be reviewed by an EMPAIA Portal Moderator.
* You will receive an email notification as soon as the request has been reviewed.

<img src="images/portal/user/00_6_register_organisation.png"></img>
