# Workbench Client 2.0

The Workbench Client 2.0 features:

* Viewing of created cases
* Viewing of all uploaded WSIs
* Running AI Apps on WSIs from a custom App UI supplied by the App Vendor

## Login to Workbench Client 2.0

* From your landing page click **WORKBENCH 2.0**.

<img src="images/workbench/21_gold_landing_workbench.png"></img>

* Click on **Login**
* You will again be forwarded to the login page

<img src="images/workbench/35_gold_workbench2_login.png"></img>

## Select a case

* Click on any of your cases

<img src="images/workbench/36_gold_workbench2_select_case.png"></img>

## Select a WSI

* All WSIs in you case will be displayed here
* Click on one of your WSIs
* Navigate on your slide by
  * panning with the selected hand tool in the toolbar or clicking on different regions on the overview map
  * zooming with the mouse wheel or by clicking on the **+** and **-** icons on the bottom right corner

<img src="images/workbench/37_gold_workbench2_select_wsi.png"></img>

## Add an App

* To run an app, it must first be added to your case
* Click on **+NEW** in the Apps panel

<img src="images/workbench/38_gold_workbench2_add_app.png"></img>

## View App in panel

* As soon as an App is selected, it is added to the Apps panel

<img src="images/workbench/39_gold_workbench2_select_app.png"></img>

## Run an App

* Click on the App you want to use in the Apps panel
* Click on **Start**

<img src="images/workbench/40_gold_workbench2_start_app_ui.png"></img>

## Use an App

Most App UIs used in the **Workbench Client 2.0** are provided by the developers of the selected AI App and are not part of the Workbench Client. Research-only Apps may offer a Generic App UI developed by the EMPAIA Team instead of a custom App UI.
