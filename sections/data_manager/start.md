# Data Manager

The Data Manager allows you to:

* Create cases (containing 0 to n WSIs)
* Delete cases
* Add WSIs to a case
* Delete WSIs from a case
* Specify stain and tissue type for each WSI

## Open the Data Manager

* From your landing page click **DATA MANAGER**

<img src="images/dmc/11_gold_landing_data_manager.png"></img>

## Login with your user

* Click on **Anmelden** at the top right corner
* You will be forwarded to the login page

<img src="images/dmc/10_gold_data_manager_login.png"></img>

## Create a new case

* After logging in, click on the **plus** sign at the right to create a new case

<img src="images/dmc/12_gold_data_manager_create_case.png"></img>

### Fill in the form for case creation

* Enter an **ID** and a **description**
* **IMPORTANT:** Do not add any sensitive patient data or any data that might be used to identify patients
* Click **CREATE CASE**

<img src="images/dmc/13_gold_data_manager_create_case.png"></img>

### Read and agree to the noitce

* Confirm that no sensitive patient data or any data that might be used to identify patients was added

<img src="images/dmc/14_gold_data_manager_create_case.png"></img>

## Uploading WSIs to a case

* To upload a WSI, drag a WSI file into the drop field
* For multi-file WSIs, drop the containing folder or multiple selected files
* For more information, click on the **?** icon in the top right corner beside your name

<img src="images/dmc/15_gold_data_manager_upload_wsi.png"></img>

### Anonymization of WSIs

* Only use anonymized WSIs
  * The Data Manager trys to anonymize WSIs during upload by removing the label and macro (if present) as well as all related metadata
  * But we do not guarantee this anonymization to work
    * E.g. in the image below
  * Therefore only use pre-anonymized WSIs or WSIs that are allowed for research by your organization
* For more information, click the **?** icon in the top right corner beside your name

<img src="images/dmc/16_gold_data_manager_upload_wsi.png"></img>

### Upload Process

* Do not close the browser or navigate to a different case during the upload

<img src="images/dmc/17_gold_data_manager_upload_wsi.png"></img>

### Add a tissue and stain tag to your WSI

* Select a tissue and stain tag for your newly uploaded WSI

<img src="images/dmc/19_gold_data_manager_upload_wsi.png"></img>

## Quick Help

* You may open the help tool box by clicking the **?** icon in the top right corner

<img src="images/dmc/18_gold_data_manager_upload_wsi_quickstart.png"></img>

