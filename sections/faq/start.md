# FAQ

This section answers the most frequently asked questions by end users (pathologists) of the EMPAIA Platform.

## What is the EMPAIA Platform?

The EMPAIA Platform serves as a real-world illustration of a system that enables storing and viewing of whole slide images, along with the application of different tools for image analysis and artificial intelligence.

## Who uses the EMPAIA Platform?

The EMPAIA Platform is used by pathologists to evaluate image analysis apps based on their own whole slide images. It is also used by app developers to integrate their apps and test the conformity with the EMPAIA specifications. Furthermore, clinical system developers use the EMPAIA Platform as a blueprint to add support for EMPAIA specifications into their own products.

## Is the EMPAIA Platform a (commercial) certified medical device software?

The EMPAIA Platform is fully functional and is used for evaluation purposes, but it is not a certified medical device software for routine usage. However, established clinical systems, such as image management systems and laboratory information systems, can integrate EMPAIA specifications to enable the same functionality in routine diagnostic use.

## What is the advantage of the EMPAIA Platform compared to existing clinical systems?

Until now, clinical systems mostly provided proprietary integrations of image analysis apps. Making such apps compatible with any given clinical system required dedicated integration projects between a particular app provider and the clinical system provider. These one-to-one integrations are costly and time consuming. The EMPAIA Platform establishes common specifications on how apps and clinical systems communicate with each other via technical protocols. This lowers the barriers to market entry for app providers to make new and existing algorithms available sooner and cheaper, benefiting all stakeholders including customers.

## Does EMPAIA develop commercial algorithms?

No, the EMPAIA Consortium enables industry partners to connect their image analysis apps with EMPAIA-compatible clinical systems. All software developed by EMPAIA serves as an example or is used for research only.

## Where can I find a list of available algorithms?

A list of available image analysis apps can be found in the [EMPAIA Portal](https://portal.empaia.org/).

## Are available algorithms certified?

The EMPAIA Platform is open for certified and research-only image analysis apps. EMPAIA industry partners are responsible for the certification of their own products. However, the EMPAIA Platform is explicitly designed to support the usage of certified products. For example, app providers can integrate their own certified User Interfaces into clinical systems to complement their algorithms. Furthermore, the EMPAIA Consortium offers *Analytical App Validation Services* to app providers for specific use cases and datasets.

## How can I evaluate the available algorithms?

All official EMPAIA reference centers (clinics and laboratories) may evaluate available (commercial) image analysis apps for free, by uploading anonymized whole slide images to a cloud-based EMPAIA Platform provided by the EMPAIA Consortium.

## How can my laboratory / clinic become an EMPAIA Reference Center?

Please contact us at [support@empaia.org](mailto:support@empaia.org).

## Can I evaluate available algorithms offline?

Yes, the EMPAIA Platform reference implementation can be installed offline (on-premises) in your clinic network by your own IT department. Please get in contact with the EMPAIA Team via [support@empaia.org](mailto:support@empaia.org) to plan such IT-related projects.

## EMPAIA is not yet available in my lab. What can I do?

1. Contact the EMPAIA Team via [support@empaia.org](mailto:support@empaia.org) about ongoing integration projects with industry partners.
2. Contact the provider of your clinical systems directly and ask for integration of EMPAIA-compatible image analysis apps.

## Can my lab buy algorithms in the EMPAIA Portal Marketplace?

No, the EMPAIA Marketplace serves as a showcase of available image analysis apps and as an example for third-party commercial marketplaces. Please contact your clinical system and app providers for commercial licenses.
