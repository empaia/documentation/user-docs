# User Documentation

Welcome to the EMPAIA User Docs. Here you will find information about:

* Frequently asked questions (FAQ) on what the EMPAIA Platform is, what it is not, and why it is relevant for pathologists
* User documentation for the EMPAIA Platform and its web-based user interfaces (Portal, Data Manager and Workbench)

## Contact

If you have questions or problems please contact us directly via [support@empaia.org](mailto:support@empaia.org).
